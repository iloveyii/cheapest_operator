const assert = require('chai').assert;
const bestPrice = require('../src/bestPrice').findLowestPrice;

describe('TEST Lowest Prices:', function () {
    let result;
    describe('Find lowest prices for number 4673879998797', function () {
        it('Has lowest price of 0.9', function () {
            const lowestPrice = 0.9;
            const fileNames = ['./src/operatora.txt', './src/operatorb.txt' ];
            const number = '4673879998797';

            result = bestPrice(fileNames, number);
            assert.equal(lowestPrice, result[1]);
        });

        it('From operator a', function () {
            assert.equal('./src/operatora.txt', result[2]);
        });
    });
});

