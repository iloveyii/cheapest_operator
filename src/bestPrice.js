const fs = require('fs');


function findLongestMatchedPrefix(operatorPrices, mobileNumber) {

    let longestMatchNumber = '';
    let longestMatchIndex = -1;

    operatorPrices.forEach(function (aPrefixPrices, index) {// console.log(p.trim());
        let prefix = aPrefixPrices[0];
        if( mobileNumber.lastIndexOf(prefix) === 0 ) {
            if(longestMatchNumber.length < prefix.length ) {
                longestMatchNumber = prefix;
                longestMatchIndex = index;
            }
        }
    });

    return longestMatchIndex > -1 ? operatorPrices[longestMatchIndex] : [];
}

function fileContentsToArray(contents) {
    // Remove tabs
    const prefixPrices = (contents.trim()).split(/\r?\n/);

    var aPrefixPrices = [];
    prefixPrices.forEach(function (prefixPrice, index) {
        var aPrefixPrice = prefixPrice.split(' ');
        aPrefixPrices[index] = [ (aPrefixPrice[0]).trim(), (aPrefixPrice[1]).trim() ];
    });

    return aPrefixPrices;
}

function findLowestPriceForOperator(filename, number) {
    let contents = fs.readFileSync(filename, 'utf8');
    let aPrefixPrices = fileContentsToArray(contents);
    const result = findLongestMatchedPrefix(aPrefixPrices, number);

    return result;
}

const findLowestPrice = function (fileNames, number) {
    let lowestPrice = 999999;
    let lowestResult = [];

    fileNames.forEach(function (filename) {
        if ( ! fs.existsSync(filename)) {
            console.log('File ' + filename + ' does not exist');
        }
        let result = findLowestPriceForOperator(filename, number);
        let operatorPrice = result.length > 0 ?  Number(result[1]) : 9999999;

        if(operatorPrice < lowestPrice) {
            lowestResult = result;
            lowestResult[2] = filename;
            lowestPrice = operatorPrice;
        }
    });

    return lowestResult;
};

// TEST DRIVE

if (process.argv.length < 4) {
    // console.log('Usage: node ' + process.argv[1] + ' NUMBER FILENAME [FILENAME...]');
    // process.exit(1);
} else {
    const number = process.argv[2];
    const fileNames = process.argv.slice(3);
    const lowestResult = findLowestPrice(fileNames, number);
    console.log('Lowest Operator: ', lowestResult);

}

module.exports = { findLowestPrice: findLowestPrice };
